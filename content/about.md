---
title: What is Your Media?

---
\# Your Media

  
For those wanting to reclaim the content that they create and share on social media, this is a project to let you easily manage your media. The best part is that all of the content is owned and controlled by You!  
This project is designed to be duplicated, modified by Forestryt.io, and deployed by Netlify.com.   
\# Starting Your Media Website  
1\. Fork this project  
2\. Login to \[Netlify.com\]([https://www.netlify.com/](https://www.netlify.com/ "https://www.netlify.com/"))- Import the forked project by clicking the "New site from Git" button and use all of the predefined settings.- Click "Deploy site". This will build this website and create a URL for it.- If you want to change this URL, going to "Site settings -> Site Information -> Site Name". This will change the URL to use your sight name.  
3\. Create an account with \[Forestry.io\]([https://forestry.io/](https://forestry.io/ "https://forestry.io/"))- Import your newly forked repo. - You will only need to worry about the top half of the content manager sidebar. Everything below "SITE" has already been set up.- From top to bottom: - Configure your site. Title, netlify URL, custom banner image, and greeting. - Write a bit about you in the About page in the Pages/ folder - Add your posts by going to the Posts section. You will find some examples that you can delete or mark as drafts to keep around.  
\# Time to Share- Any Post you write will trigger your site to be updated shortly.- You can then share the link to your post on any website you wish.- Also, full RSS support already exist for your home page or any of your tags, so you can let your family follow your "cats" posts with the easy of any RSS reader.