+++
date = "2019-04-21T13:24:00+00:00"
description = "Simple post"
img_src = ""
tags = ["Fun"]
title = "My first post"

+++
I hope everyone can take back their media!

Markdown

\-------------  
...is really just ordinary text, _plain and simple_. How is it good for you?  
\- You just **type naturally**, and the result looks good.  
\- You **don't have to worry** about clicking formatting buttons. 

\- Or fiddling with indentation. (Two spaces is all you need.)  
To see what else you can do with Markdown (including **tables**, **images**, **numbered lists**, and more) take a look at the \[Cheatsheet\]\[1\]. And then try it out by typing in this box!  
\[1\]: [https://github.com/adam-p/markdown-here/wiki/Markdown-Here-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Here-Cheatsheet "https://github.com/adam-p/markdown-here/wiki/Markdown-Here-Cheatsheet")