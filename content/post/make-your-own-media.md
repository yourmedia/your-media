+++
date = "2019-04-21T14:13:17+00:00"
description = "Detailed tutorial on how to start making your own media."
img_src = ""
tags = ["blog", "social media"]
title = "Make Your own Media"

+++
# Making Your Media site

With this walk-through I hope that you will be able to build and manage your own media website to control what you share with others.

### Import the project

{{< rawhtml >}}

<a href="https://app.forestry.io/quick-start?repo=vordimous/your-media&provider=gitlab&engine=hugo&version=0.54">

<img alt="Import this project into Forestry" src="https://assets.forestry.io/import-to-forestry.svg" />

</a>

{{< /rawhtml >}}

* **Create** a [Gitlab account](https://gitlab.com/users/sign_in#register-pane "Gitlab account") or login if you already have one.

### Setup the build

* **Go to** [Netlify.com](https://www.netlify.com/ "https://www.netlify.com/") and sign up using your Gitlab account and authorize Netlify to access it.

  {{< figure src="/img/2019-04-21 09_06_16-Welcome to Netlify _ Netlify.png" >}}

  {{< figure src="/img/2019-04-21 09_06_46-User Settings · GitLab.png" >}}
* You can **Skip** this quick start guide.

  {{< figure src="/img/2019-04-21 09_07_18-Sites _ Your Media's team.png" >}}
* Now **Import** your site.

  {{< figure src="/img/2019-04-21 09_07_28-Sites _ Your Media's team.png" >}}
* Select **Gitlab** as your source.

  {{< figure src="/img/2019-04-21 09_07_38-Create a new site _ Netlify.png" >}}
* Find and select the **Your Media** project.

  {{< figure src="/img/2019-04-21 09_07_55-Create a new site _ Netlify.png" >}}
* **Don't change** any of the settings and click **Deploy site**.

  {{< figure src="/img/2019-04-21 09_08_23-Create a new site _ Netlify.png" >}}
* Your site will start deploying.

  {{< figure src="/img/2019-04-21 09_08_48-Overview _ sleepy-joliot-16c395.png" >}}
* Once it is finished you can change the name in **Site settings**.

  {{< figure src="/img/2019-04-21 09_09_10-Overview _ sleepy-joliot-16c395.png" >}}
* Change the **Site Name** to be whatever you want.

  {{< figure src="/img/2019-04-21 09_09_26-General _ Settings.png" >}}
* Once changed you will want to **copy** the new site **URL**  for later use.

  {{< figure src="/img/2019-04-21 09_09_54-General _ Settings.png" >}}

### Manage Your Media!

* Your site will be managed throung [Forestry.io](https://forestry.io/ "https://forestry.io/"). Go there and login using your **Gitlab account** again.

  {{< figure src="/img/2019-04-21 09_12_25-Forestry.io.png" >}}
* You will now need to **Add your site**.

  {{< figure src="/img/2019-04-21 09_12_42-Forestry.io.png" >}}
* When prompted you will need to select a **HUGO** version **0.54.0** import.

  {{< figure src="/img/2019-04-21 09_13_23-Forestry.io.png" >}}
* Again select **Gitlab**.

  {{< figure src="/img/2019-04-21 09_13_36-Forestry.io.png" >}}
* Now find **Your Media** project in the list and select the **Master** branch.

  {{< figure src="/img/2019-04-21 09_14_03-Forestry.io.png" >}}
* You can skip Inviting Guests and click **Import Site**.

  {{< figure src="/img/2019-04-21 09_14_16-Forestry.io.png" >}}
* Now your manager is created and all of your configuration is done so you can **skip setup** by clicking the **mark as done** options then **complete setup**.

  {{< figure src="/img/2019-04-21 09_14_46-forestry.io.png" >}}

  {{< figure src="/img/2019-04-21 09_15_04-forestry.io.png" >}}
* Everything is setup and ready for changes. In the sidebar you are only worried about the top 3 items. Config, Pages, and Posts

  {{< figure src="/img/2019-04-21 09_15_16-forestry.io.png" >}}

#### Config

* Change your **Title**. Past your **URL** that you coppied from netlify.

  {{< figure src="/img/2019-04-21 09_16_02-forestry.io.png" >}}
* By clicking on **Params** you can change your banner image and your greeting

  {{< figure src="/img/2019-04-21 09_16_13-forestry.io.png" >}}

#### Pages

* You only have one page that is your **About** page. Here you can put as much detail about yourself or Your Media as you would like.

  {{< figure src="/img/2019-04-21 21_48_58-forestry.io.png" >}}
* Be sure to **Save** any changes you make.

  {{< figure src="/img/2019-04-21 09_18_39-.png" >}}

#### Posts

* The majority of your content will be in a **Post**. There will be some example posts showing you some of what is capable here. You can delete these or **Mark then as Drafts** so that they don't show up on your site.
* Start a new post by clicking **Create new** -> **Posts**.

  {{< figure src="/img/2019-04-21 09_23_48-forestry.io.png" >}}
* Fill out the post form. You will want at least a **Title**. The **tags** field is for grouping your posts and will help split up you rss feeds. The **Content Image** and **Main body** will make up your post. Remember to turn **Draft off** and **Save**.

  {{< figure src="/img/2019-04-21 21_48_58-forestry.io.png" >}}
* You content will **automatically** update your site and should show up within **minutes**.
* YOUR DONE! You have made your first post.

  {{< figure src="/img/2019-04-21 10_17_13-Your Media _ Your Media.png" >}}